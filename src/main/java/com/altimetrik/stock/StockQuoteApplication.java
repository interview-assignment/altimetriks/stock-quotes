package com.altimetrik.stock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages={"com.altimetrik"})
public class StockQuoteApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockQuoteApplication.class, args);
	}

}
