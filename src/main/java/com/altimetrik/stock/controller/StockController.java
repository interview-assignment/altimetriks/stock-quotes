package com.altimetrik.stock.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.altimetrik.stock.exception.ResourceNotFoundException;
import com.altimetrik.stock.model.Stocks;
import com.altimetrik.stock.repository.StockRepository;

@RestController
public class StockController {

	@Autowired
	private StockRepository stockRepository;

	@GetMapping("/Stocks")
	public ResponseEntity<List<Stocks>> getStockById(@RequestParam("value") String value)
			throws ResourceNotFoundException {
		if (value == null || value.equals(""))
			throw new ResourceNotFoundException("Exception : Stock not found for empty values");
		List<String> list = Stream.of(value.split(",")).collect(Collectors.toList());
		
		List <Stocks> stocks = stockRepository.findByStockQuoteIn(list);
		if(stocks.isEmpty()) 
			throw new ResourceNotFoundException("Stock not found ");
		return ResponseEntity.ok().body(stocks);
	}

	@PostMapping("/Stocks")
	public Stocks createStock(@RequestBody Stocks stocks) {
		
		/*
		 * if (list.size() > 10) throw new
		 * ResourceNotFoundException("Exception : Trying to fetch more then 10 stock data"
		 * );
		 */		
		return stockRepository.save(stocks);
		
	}

}
