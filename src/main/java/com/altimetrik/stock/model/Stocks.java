package com.altimetrik.stock.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "stocks")
public class Stocks  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id;
	private String stockQuote;
	private String companyName;
	@JsonFormat(pattern="dd-MM-yy")
	private LocalDate date;
	private double price;
	private String currency;

	public Stocks() {

	}

	public Stocks(long id, String stockQuote, String companyName, LocalDate date, double price, String currency) {
		this.id = id;
		this.stockQuote = stockQuote;
		this.companyName = companyName;
		this.date = date;
		this.price = price;
		this.currency = currency;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "stock_quote")
	public String getStockQuote() {
		return stockQuote;
	}

	public void setStockQuote(String stockQuote) {
		this.stockQuote = stockQuote;
	}

	@Column(name = "company_name")
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Column(name = "date")
	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	@Column(name = "price")
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Column(name = "currency")
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "Stocks [ stockQuote=" + stockQuote + ", companyName=" + companyName + ", date=" + date
				+ ", price=" + price + ", currency=" + currency + "]";
	}

	
	
}
