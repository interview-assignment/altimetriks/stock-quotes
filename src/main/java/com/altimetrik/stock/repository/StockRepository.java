package com.altimetrik.stock.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.altimetrik.stock.model.Stocks;

@Repository
public interface StockRepository extends JpaRepository<Stocks, Long>{

	List<Stocks> findByStockQuoteIn(List<String> stockQuotes);
	
}
