DROP TABLE IF EXISTS stocks;
	
CREATE TABLE stocks (
	  id INT  PRIMARY KEY auto_increment,
	  stock_quote VARCHAR(255) NOT NULL,
	  company_name VARCHAR(255) NOT NULL,
	  date DATE,
	  price DECIMAL(14,2),
	  currency VARCHAR(3)
	);
	

Insert into STOCKS(STOCK_QUOTE,COMPANY_NAME,DATE,PRICE,CURRENCY) values ('HCL','HCL Tech',now(),200.20,'INR');
	
	

